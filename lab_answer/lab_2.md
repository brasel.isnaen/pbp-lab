1. Apakah perbedaan antara JSON dan XML?
    JSON adalah *JavaScript Object Notation*
    Penyimpanan data pada JSON berupa map yang berisi *key* dan *value*.
    JSON tidak melakukan pemrosesan, tetapi hanya melakukan transfer data saja.
    Karena format yang lebih simpel, JSON memiliki ukuran dokumen dan kecepatan transfer yang relatif lebih kecil dan lebih cepat.

    XML adalah *eXtensible Markup Language*, yang berarti bukan sebuah bahasa pemrograman, melainkan bahasa markup, seperti html.
    Penyimpanan data pada XML berupa *tree structure*, yang harus memiliki sebuah *root element*
    XML dapat pemrosesan data pada dokumen maupun objek
    Format data XML lebih banyak dan rumit, mengakibatkan ukuran dokumen yang lebih besar, pengolahan data yang lebih lambat, dan kecepatan transfer yang lebih lambat.

2. Apakah perbedaan antara HTML dan XML?
    HTML adalah *HyperText Markup Language*
    HTML statis, sehingga hanya digunakan untuk menampilkan data
    HTML tidak dapat menyimpan dan mentransfer data
    Tags pada HTML sudah ditentukan dan tidak dapat dikembangkan
    Tujuan dari HTML adalah menafsirkan bagaimana sebuah format data apabila ditampilkan sebagai bahasa/tampilan yang dapat dimengerti manusia

    XML adalah *eXtensible Markup Language*
    XML dapat melakukan penyimpanan dan transfer data
    XML memungkinkan untuk mengembangkan tags
    XML memungkinkan fleksibilitas dalam pembagian data-data secara elektronik melalui jaringan

    referensi 
    https://blogs.masterweb.com/perbedaan-xml-dan-html/ 
    https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html 