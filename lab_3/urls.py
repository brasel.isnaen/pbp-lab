from lab_3.views import index, add_friend
from django.urls import path
# from .views import index

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name = 'add')
]
