from lab_1.models import Friend
from django import forms
# from lab_3.models import add_friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        