import 'dart:ui';

import 'package:flutter/material.dart';

@immutable
class AppColors {
  final white = const Color(0xFFFFFFFF);

  const AppColors();
}