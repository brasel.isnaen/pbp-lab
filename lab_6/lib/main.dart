import 'package:flutter/material.dart';
import 'package:lab_6/constants.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: const Color(0xFFFFFFFF),
        fontFamily: 'Georgia',
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 5.0),
        ),
      ),
      home: Scaffold(
        appBar: AppBar(title: Text("Lab 6"),),
        body: Row(
          children: <Widget>[
            Column(children: <Widget>[
              Container(
                height: 200,
                width: 180,
                decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.secondary,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Image.asset('assets/images/gloves.png'),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Text("Sarung Tangan"),
            ),
            ],
            ),
           Column(children: <Widget>[
              Container(
              height: 200,
              width: 180,
              
              decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
              ),
              child: Image.asset('assets/images/hearingaid.png'),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Text(
              "Hearing Aid",
              ),
            ),
            ],
            ),
          ],
        ),
      
      ),
    );
  }
}
