from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note


def index(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'index_lab2.html', response)

def xml():
    notes = Note.objects.all()
    notes_xml = serializers.serialize('xml', notes)
    return HttpResponse(notes_xml, content_type="application/xml")

def json():
    notes = Note.objects.all()
    notes_json = serializers.serialize('json', notes)
    return HttpResponse(notes_json, content_type="application/json")
