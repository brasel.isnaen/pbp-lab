import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String title = 'Lab 7';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primaryColor: Colors.lightBlue),
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String kesan = '';
  String pesan = '';

  Widget Judul() => const Text(
        "Form Kesan Pesan",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      );

  //Referensi : https://github.com/boriszv/Programming-Addict-Code-Examples/blob/master/flutter_forms/lib/main.dart
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Form(
          key: formKey,
          child: ListView(
            padding: const EdgeInsets.all(16),
            children: [
              Judul(),
              const SizedBox(height: 8),
              textNama(),
              const SizedBox(height: 8),
              textKesan(),
              const SizedBox(height: 8),
              textPesan(),
              const SizedBox(height: 32),
              buildSubmit(),
            ],
          ),
        ),
      );

  Widget textNama() => TextFormField(
        decoration: const InputDecoration(
          labelText: 'Nama',
          border: OutlineInputBorder(),
        ),
        maxLength: 50,
        onSaved: (value) => setState(() => nama = value!),
      );

  Widget textKesan() => TextFormField(
        decoration: const InputDecoration(
          labelText: 'Kesan',
          border: OutlineInputBorder(),
        ),
        maxLength: 50,
        onSaved: (value) => setState(() => kesan = value!),
      );

  Widget textPesan() => TextFormField(
        decoration: const InputDecoration(
          labelText: 'Pesan',
          border: OutlineInputBorder(),
        ),
        maxLength: 50,
        onSaved: (value) => setState(() => pesan = value!),
      );

  Widget buildSubmit() => TextButton(
        child: const Text("Kirim"),
        onPressed: () {
          final isValid = formKey.currentState!.validate();

          if (isValid) {
            formKey.currentState!.save();

            final message = '$nama berhasil menambahkan kesan: $kesan dan pesan: $pesan';

            final snackBar = SnackBar(
              content: Text(
                message,
                style: TextStyle(fontSize: 20),
              ),
              backgroundColor: Colors.green,
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        style: TextButton.styleFrom(
            primary: Colors.white, backgroundColor: Colors.lightBlue),
      );
}
