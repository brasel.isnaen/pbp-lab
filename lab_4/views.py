from django.shortcuts import redirect, render
from lab_2.models import Note
from django.contrib.auth.decorators import login_required
from lab_4.forms import NoteForm
# Create your views here.xx
def index(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/lab-4')
        
    response = {'form' : form}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_note_list.html', response)